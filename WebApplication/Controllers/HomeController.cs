﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApplication.Models;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;

namespace WebApplication.Controllers
{
    public class HomeController : Controller
    {
        private SiteContext db;

        public HomeController(SiteContext context)
        {
            db = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Links()
        {
            return View();
        }

        public IActionResult AddUser(UserModel userModel)
        {
            var found = db.Users.FirstOrDefault(u => u.Login == userModel.Login || u.Email == userModel.Email);
            if (found != null)
            {
                ModelState.AddModelError("",
                    found.Login == userModel.Login
                        ? $"Логин {userModel.Login} уже используется!"
                        : $"Почта {userModel.Email} уже используется!");
                return View();
            }

            userModel.CreatedAt = DateTime.Now;
            db.Users.Add(userModel);
            db.SaveChanges();
            return View(userModel);
        }

        [Authorize(Roles = "admin")]
        public IActionResult ListUsers()
        {
            return View(db.Users.OrderBy(b => b.FullName).ToList());
        }

        [HttpGet]
        public IActionResult AddUser()
        {
            return View();
        }

        public IActionResult GetProjectById()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                UserModel user = db.Users.FirstOrDefault(u => u.Login == model.Login && u.Password == model.Password);
                if (user != null)
                {
                    await Authenticate(user.Role, user.Login); // аутентификация
                    return RedirectToAction("Index", "Home");
                }

                ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            }
            else
                ModelState.AddModelError("", "Некорректные логин и(или) пароль");

            return View(model);
        }


        private async Task Authenticate(string role, string userLogin)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, userLogin),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, role == "admin" ? "admin" : "user")
            };
            // создаем объект ClaimsIdentity
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
            // установка аутентификационных cookies
            await HttpContext.SignInAsync("Cookies", new ClaimsPrincipal(id));
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync("Cookies");
            return RedirectToAction("Index", "Home");
        }


        [HttpGet]
        public ActionResult AddComment()
        {
            
            IEnumerable<CommentModel> query = GetComments();
            ViewBag.Comments = query;
            foreach (var item in query)
                db.Entry(item).Reference(p => p.UserModel).Load();
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult AddComment(CommentModel commentModel)
        {

            IEnumerable<CommentModel> query = GetComments();
            ViewBag.Comments = query;
            foreach (var item in query)
                db.Entry(item).Reference(p => p.UserModel).Load();
            var userModel = db.Users.FirstOrDefault(u => u.Login == User.Identity.Name);
            commentModel.CommentDate = DateTime.Now;
            commentModel.UserModel = userModel;
            db.Comments.Add(commentModel);
            db.SaveChanges();
            return View();
        }

        public ActionResult HideComment(int id)
        {
            CommentModel comment = db.Comments.Find(id);
            if (comment != null)
            {
                comment.Hidden = !comment.Hidden;
                db.SaveChanges();
            }

            return RedirectToAction("AddComment", "Home");
        }

        public ActionResult EditCommentView(int id)
        {
            IEnumerable<CommentModel> query = GetComments();
            ViewBag.Comments = query;
            foreach (var item in query) 
                db.Entry(item).Reference(p => p.UserModel).Load(); 
            CommentModel comment = db.Comments.Find(id); 
            ViewBag.EditId = id; 
            return View("~/Views/Home/AddComment.cshtml", comment); 
        }

        public ActionResult EditComment(int id,
            CommentModel commentModel)
        {
            var commet = db.Comments.Find(id);
            commet.CommentText = commentModel.CommentText;
            db.Comments.Update(commet);
            db.SaveChanges();
            return RedirectToAction("AddComment", "Home");
        }
        private IEnumerable<CommentModel> GetComments()
        {
            IEnumerable<CommentModel> query;
            if (User.Identity.IsAuthenticated && User.IsInRole("admin"))
            {
                query = from b in db.Comments
                    orderby b.CommentDate descending
                    select b;
            }
            else
            {
                query = from b in db.Comments
                    where !b.Hidden
                    orderby b.CommentDate descending
                    select b;
            }

            return query;
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}