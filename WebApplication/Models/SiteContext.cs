﻿using Microsoft.EntityFrameworkCore; 

namespace WebApplication.Models
{
    public class SiteContext : DbContext
    {
        public DbSet<UserModel> Users { get; set; }
        public DbSet<CommentModel> Comments { get; set; } 

        public SiteContext(DbContextOptions<SiteContext> options) : base(options)
        {
        }
    }
}