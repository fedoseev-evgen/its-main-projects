﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace WebApplication.Models
{
    public class CommentModel
    {
        public int Id { get; set; }
        public DateTime CommentDate { get; set; }
        [Required] 
        public string CommentText { get; set; }
        
        [DefaultValue(true)]
        public bool Hidden { get; set; }
        public UserModel UserModel { get; set; }
    }
}