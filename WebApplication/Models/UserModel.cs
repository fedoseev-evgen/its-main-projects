﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication.Models
{
    public class UserModel
    {
        public int Id { get; set; } 
        [Required]
        [RegularExpression("^[А-ЯЁ][а-яё]+ [А-ЯЁ][а-яё]+ [А-ЯЁ][а-яё]+$")]
        public string FullName { set; get; }

        [Required]
        [RegularExpression("^[0-9]{3}-[0-9]{3}-[0-9]{2}-[0-9]{2}$")]
        public string PhoneNumber { set; get; }

        [Required] public string Email { set; get; }
        [Required] public string Login { set; get; }
        [Required] public string Password { set; get; }
        
        [NotMapped] public string ResetPassword { set; get; }
        [Required] public string Sex { set; get; }
        [Required] public int Year { set; get; }
        [DefaultValue("user")]
        public string Role { set; get; }
        public DateTime CreatedAt { set; get; }
        
        public ICollection<CommentModel> CommentModel { get; set; }
    }
}